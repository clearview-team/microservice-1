# Microservice #1

This task is 90% backend, and 10% frontend - can be used for Full Stack assessments.

Develop a simple URL shortener service which will feature the following:

 - Ability to add new URL's for redirection
 - Each new URL will get their own unique identifiers
 - Store as much as metadata you can for all newly created URL's (ip, browser, architecture, geolocation is a bonus, ...)
 - Create 2 milion dummy URL items in the database with unique identifiers (duplicates allowed for URLs)
 - Redirect each item to their destination URL
 - Allow users to set a password for their URLs (will be prompted before they redirect, you will need to create a nonce or a token for each passworded attempt to redirect, using just a password will not unlock the redirection)
 - Add ability to set URL item expiration date and time, after which the specified URL items will give back a 404 Not Found HTTP Status
 - Create the most simple UI to add new URLs with CORS and anti-spam in mind
 - Create a custom anti-spam system to allow 15 entries in 5 minutes for a specific signature: IP + Browser

**Further developments:**

 - Create a crontab task, or any form of automated script to constantly add new dummy URLs into the database to mock real production environments (the more in a moment, the better)
 - Create another automated process which will have a pre-defined list from previously created URL items to actually execute their redirection (the more in a moment, the better)
 - Try to reduce loads

**Requirements:**

 - Achieve this in either Node.js with or without TypeScript, C# or Java
 - Create endpoints for each functionality
 - Introduce a seamless setup process
 - Maintain a proper and reasonable project structure
 - Separate business logic from the presentation
 - Rely if can on OOP principles, like SOLID and implement any design patterns you find reasonable
 - Deliver the solution in a form of a Git repository: GitHub, GitLab, Bitbucket, ...

**Special bonuses:**

 - Dockerization
 - API documentation
 - Authorization system with JWT (login, register, password recovery, etc.)
 - Logging service, to detect which URLs are faulty and not redirecting properly
 - Indexed search through millions of records
